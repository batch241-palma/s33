/* ACTIVITY S33 */

/* No.3 */
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => data.map((list) => {
    return list.title 
  }))
/* No.4 */
.then((list) => console.log(list));

/* No.5 */
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
/* No.6 */
.then((json) => {
	console.log(json)
	console.log('The item "' + json.title + '" on the list has a status of ' + json.completed);});

/* No.7 */
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
    	"userId": 1,
    	"title": "Created to do list",
    	"completed": false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/* No.8 */
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	/* No.9 */
	body: JSON.stringify({
		"title": "Created to do list",
		"description": "To update the my to do list with a different data structure",
		"status": "Pending",
		"dataCompleted": "Pending",
		"userId": 1	
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/* No.10*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"title": "delectus aut autem",
		"completed": false,
		"status": "Complete",
		"dataCompleted": "07/09/21",
		"userId": 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
});


/* END OF ACTIVITY S33 */